# FlacNet

This repository hosts a simple .NET Core 3.0 wrapper around the native [libflac](https://github.com/xiph/flac) C library. It was quickly created in the need for lean decoding of FLAC samples to stream them in a game written in C#.

# Compiling

FlacNet currently requires libflac 1.1.3 or newer, compiled as a x64 dynamic library. The resulting library file has to be named "libflac" (removing the `_dynamic` suffix). Detailed libflac build instructions are found in the official repository readme.
