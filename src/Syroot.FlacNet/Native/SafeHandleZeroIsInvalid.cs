﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.FlacNet.Native
{
    /// <summary>
    /// Represents a <see cref="SafeHandle"/> which is invalid if the handle is 0.
    /// </summary>
    internal abstract class SafeHandleZeroIsInvalid : SafeHandle
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        protected SafeHandleZeroIsInvalid(bool ownsHandle) : base(IntPtr.Zero, ownsHandle) { }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override bool IsInvalid => handle == IntPtr.Zero;
    }
}
