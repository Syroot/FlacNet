﻿using System.Runtime.InteropServices;

namespace Syroot.FlacNet.Native
{
    /// <summary>
    /// Represents a <see cref="SafeHandle"/> for a FLAC__StreamDecoder.
    /// </summary>
    internal class FLAC__StreamDecoder : SafeHandleZeroIsInvalid
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        protected FLAC__StreamDecoder() : base(true) { }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override bool ReleaseHandle()
        {
            LibFlac.FLAC__stream_decoder_delete(this);
            return true;
        }
    }
}
