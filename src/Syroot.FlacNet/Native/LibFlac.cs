﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.FlacNet.Native
{
    /// <summary>
    /// Represents definitions of the native libFLAC C library.
    /// </summary>
    internal static unsafe class LibFlac
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        internal const int FLAC__MAX_CHANNELS = 8;
        internal const int FLAC__MAX_FIXED_ORDER = 4;
        internal const int FLAC__MAX_LPC_ORDER = 32;

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        [DllImport(nameof(LibFlac), CallingConvention = CallingConvention.Cdecl)]
        internal static extern void FLAC__stream_decoder_delete(FLAC__StreamDecoder decoder);

        [DllImport(nameof(LibFlac), CallingConvention = CallingConvention.Cdecl)]
        internal static extern FLAC__StreamDecoder FLAC__stream_decoder_new();

        [DllImport(nameof(LibFlac), CallingConvention = CallingConvention.Cdecl)]
        internal static extern FLAC__StreamDecoderInitStatus FLAC__stream_decoder_init_stream(
            FLAC__StreamDecoder decoder,
            FLAC__StreamDecoderReadCallback read_callback,
            FLAC__StreamDecoderSeekCallback seek_callback,
            FLAC__StreamDecoderTellCallback tell_callback,
            FLAC__StreamDecoderLengthCallback length_callback,
            FLAC__StreamDecoderEofCallback eof_callback,
            FLAC__StreamDecoderWriteCallback write_callback,
            FLAC__StreamDecoderMetadataCallback metadata_callback,
            FLAC__StreamDecoderErrorCallback error_callback,
            IntPtr client_data);

        [DllImport(nameof(LibFlac), CallingConvention = CallingConvention.Cdecl)]
        internal static extern bool FLAC__stream_decoder_process_until_end_of_metadata(FLAC__StreamDecoder decoder);

        [DllImport(nameof(LibFlac), CallingConvention = CallingConvention.Cdecl)]
        internal static extern bool FLAC__stream_decoder_process_until_end_of_stream(FLAC__StreamDecoder decoder);

        [DllImport(nameof(LibFlac), CallingConvention = CallingConvention.Cdecl)]
        internal static extern FLAC__StreamDecoderState FLAC__stream_decoder_get_state(FLAC__StreamDecoder decoder);

        [DllImport(nameof(LibFlac), CallingConvention = CallingConvention.Cdecl)]
        internal static extern bool FLAC__stream_decoder_process_single(FLAC__StreamDecoder decoder);

        [DllImport(nameof(LibFlac), CallingConvention = CallingConvention.Cdecl)]
        internal static extern bool FLAC__stream_decoder_seek_absolute(FLAC__StreamDecoder decoder, ulong sample);

        // ---- DELEGATES ----------------------------------------------------------------------------------------------

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        internal delegate bool FLAC__StreamDecoderEofCallback(
            IntPtr decoder,
            IntPtr client_data);

        internal delegate void FLAC__StreamDecoderErrorCallback(
            IntPtr decoder,
            FLAC__StreamDecoderErrorStatus status,
            IntPtr client_data);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        internal delegate FLAC__StreamDecoderLengthStatus FLAC__StreamDecoderLengthCallback(
            IntPtr decoder,
            ulong* stream_length,
            IntPtr client_data);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        internal delegate void FLAC__StreamDecoderMetadataCallback(
            IntPtr decoder,
            FLAC__StreamMetadata metadata,
            IntPtr client_data);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        internal delegate FLAC__StreamDecoderReadStatus FLAC__StreamDecoderReadCallback(
            IntPtr decoder,
            byte* buffer,
            uint* bytes,
            IntPtr client_data);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        internal delegate FLAC__StreamDecoderSeekStatus FLAC__StreamDecoderSeekCallback(
            IntPtr decoder,
            ulong absolute_byte_offset,
            IntPtr client_data);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        internal delegate FLAC__StreamDecoderTellStatus FLAC__StreamDecoderTellCallback(
            IntPtr decoder,
            ulong* absolute_byte_offset,
            IntPtr client_data);

        internal delegate FLAC__StreamDecoderWriteStatus FLAC__StreamDecoderWriteCallback(
            IntPtr decoder,
            FLAC__Frame* frame,
            int** buffer, // int* buffer[]
            IntPtr client_data);

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__EntropyCodingMethod
        {
            internal FLAC__EntropyCodingMethodType type;
            internal FLAC__EntropyCodingMethod_PartitionedRice partitioned_rice;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__EntropyCodingMethod_PartitionedRice
        {
            internal uint order;
            internal FLAC__EntropyCodingMethod_PartitionedRiceContents* contents;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__EntropyCodingMethod_PartitionedRiceContents
        {
            internal uint* parameters;
            internal uint* raw_bits;
            internal uint capacity_by_order;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__Frame
        {
            internal FLAC__FrameHeader header;
            internal FLAC__Subframe subframes0; // originally subframes[FLAC__MAX_CHANNELS]
            internal FLAC__Subframe subframes1;
            internal FLAC__Subframe subframes2;
            internal FLAC__Subframe subframes3;
            internal FLAC__Subframe subframes4;
            internal FLAC__Subframe subframes5;
            internal FLAC__Subframe subframes6;
            internal FLAC__Subframe subframes7;
            internal FLAC__Subframe subframes8;
            internal FLAC__FrameFooter footer;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__FrameFooter
        {
            internal ushort crc;
        }

        [StructLayout(LayoutKind.Explicit)]
        internal struct FLAC__FrameHeader
        {
            [FieldOffset(0)] internal uint blocksize;
            [FieldOffset(4)] internal uint sample_rate;
            [FieldOffset(8)] internal uint channels;
            [FieldOffset(12)] internal FLAC__ChannelAssignment channel_assignment;
            [FieldOffset(16)] internal uint bits_per_sample;
            [FieldOffset(20)] internal FLAC__FrameNumberType number_type;
            [FieldOffset(24)] internal uint frame_number;
            [FieldOffset(24)] internal ulong sample_number;
            [FieldOffset(32)] internal byte crc;
        }

        [StructLayout(LayoutKind.Explicit)]
        internal struct FLAC__StreamMetadata
        {
            [FieldOffset(0)] internal FLAC__MetadataType type;
            [FieldOffset(4)] internal bool is_last;
            [FieldOffset(8)] internal uint length;
            [FieldOffset(16)] internal FLAC__StreamMetadata_StreamInfo stream_info;
            [FieldOffset(16)] internal FLAC__StreamMetadata_Padding padding;
            [FieldOffset(16)] internal FLAC__StreamMetadata_Application application;
            [FieldOffset(16)] internal FLAC__StreamMetadata_SeekTable seek_table;
            [FieldOffset(16)] internal FLAC__StreamMetadata_VorbisComment vorbis_comment;
            [FieldOffset(16)] internal FLAC__StreamMetadata_CueSheet cue_sheet;
            [FieldOffset(16)] internal FLAC__StreamMetadata_Picture picture;
            [FieldOffset(16)] internal FLAC__StreamMetadata_Unknown unknown;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__StreamMetadata_Application
        {
            internal fixed byte id[4];
            internal byte* data;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__StreamMetadata_CueSheet
        {
            internal fixed char media_catalog_number[129];
            internal ulong lead_in;
            internal bool is_cd;
            internal uint num_tracks;
            internal FLAC__StreamMetadata_CueSheet_Track tracks;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__StreamMetadata_CueSheet_Index
        {
            internal ulong offset;
            internal byte number;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__StreamMetadata_CueSheet_Track
        {
            internal ulong offset;
            internal byte number;
            internal fixed char isrc[13];
            internal uint type;
            internal uint pre_emphasis;
            internal byte num_indices;
            internal FLAC__StreamMetadata_CueSheet_Index* indices;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__StreamMetadata_Padding { }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__StreamMetadata_Picture
        {
            internal FLAC__StreamMetadata_Picture_Type type;
            internal char* mime_type;
            internal byte* description;
            internal uint width;
            internal uint height;
            internal uint depth;
            internal uint colors;
            internal uint data_length;
            internal byte* data;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__StreamMetadata_SeekPoint
        {
            internal ulong sample_number;
            internal ulong stream_offset;
            internal uint frame_samples;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__StreamMetadata_SeekTable
        {
            internal uint num_points;
            internal FLAC__StreamMetadata_SeekPoint* points;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 8)]
        internal struct FLAC__StreamMetadata_StreamInfo
        {
            internal uint min_blocksize;
            internal uint max_blocksize;
            internal uint min_framesize;
            internal uint max_framesize;
            internal uint sample_rate;
            internal uint channels;
            internal uint bits_per_sample;
            internal ulong total_samples;
            internal fixed byte md5sum[16];
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__StreamMetadata_Unknown
        {
            internal byte* data;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__StreamMetadata_VorbisComment
        {
            internal FLAC__StreamMetadata_VorbisComment_Entry vendor_string;
            internal uint num_comments;
            internal FLAC__StreamMetadata_VorbisComment_Entry* comments;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__StreamMetadata_VorbisComment_Entry
        {
            internal uint length;
            internal byte* entry;
        }

        [StructLayout(LayoutKind.Explicit)]
        internal struct FLAC__Subframe
        {
            [FieldOffset(0)] internal FLAC__SubframeType type;
            [FieldOffset(4)] internal FLAC__Subframe_Constant constant;
            [FieldOffset(4)] internal FLAC__Subframe_Fixed @fixed;
            [FieldOffset(4)] internal FLAC__Subframe_LPC lpc;
            [FieldOffset(4)] internal FLAC__Subframe_Verbatim verbatim;
            [FieldOffset(276)] internal uint wasted_bits;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__Subframe_Constant
        {
            internal int value;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__Subframe_Fixed
        {
            internal FLAC__EntropyCodingMethod entropy_coding_method;
            internal uint order;
            internal fixed int warmup[FLAC__MAX_FIXED_ORDER];
            internal int* residual;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__Subframe_LPC
        {
            internal FLAC__EntropyCodingMethod entropy_coding_method;
            internal uint order;
            internal uint qlp_coeff_precision;
            internal int quantization_level;
            internal fixed int qlp_coeff[FLAC__MAX_LPC_ORDER];
            internal fixed int warmup[FLAC__MAX_LPC_ORDER];
            internal int* residual;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLAC__Subframe_Verbatim
        {
            internal int* data;
        }

        internal enum FLAC__ChannelAssignment
        {
            FLAC__CHANNEL_ASSIGNMENT_INDEPENDENT,
            FLAC__CHANNEL_ASSIGNMENT_LEFT_SIDE,
            FLAC__CHANNEL_ASSIGNMENT_RIGHT_SIDE,
            FLAC__CHANNEL_ASSIGNMENT_MID_SIDE
        }

        internal enum FLAC__EntropyCodingMethodType
        {
            FLAC__ENTROPY_CODING_METHOD_PARTITIONED_RICE,
            FLAC__ENTROPY_CODING_METHOD_PARTITIONED_RICE2
        }

        internal enum FLAC__FrameNumberType
        {
            FLAC__FRAME_NUMBER_TYPE_FRAME_NUMBER,
            FLAC__FRAME_NUMBER_TYPE_SAMPLE_NUMBER
        }

        internal enum FLAC__MetadataType
        {
            FLAC__METADATA_TYPE_STREAMINFO,
            FLAC__METADATA_TYPE_PADDING,
            FLAC__METADATA_TYPE_APPLICATION,
            FLAC__METADATA_TYPE_SEEKTABLE,
            FLAC__METADATA_TYPE_VORBIS_COMMENT,
            FLAC__METADATA_TYPE_CUESHEET,
            FLAC__METADATA_TYPE_PICTURE,
            FLAC__METADATA_TYPE_UNDEFINED
        }

        internal enum FLAC__StreamDecoderErrorStatus
        {
            FLAC__STREAM_DECODER_ERROR_STATUS_LOST_SYNC,
            FLAC__STREAM_DECODER_ERROR_STATUS_BAD_HEADER,
            FLAC__STREAM_DECODER_ERROR_STATUS_FRAME_CRC_MISMATCH,
            FLAC__STREAM_DECODER_ERROR_STATUS_UNPARSEABLE_STREAM
        }

        internal enum FLAC__StreamDecoderLengthStatus
        {
            FLAC__STREAM_DECODER_LENGTH_STATUS_OK,
            FLAC__STREAM_DECODER_LENGTH_STATUS_ERROR,
            FLAC__STREAM_DECODER_LENGTH_STATUS_UNSUPPORTED
        }

        internal enum FLAC__StreamDecoderInitStatus
        {
            FLAC__STREAM_DECODER_INIT_STATUS_OK,
            FLAC__STREAM_DECODER_INIT_STATUS_UNSUPPORTED_CONTAINER,
            FLAC__STREAM_DECODER_INIT_STATUS_INVALID_CALLBACKS,
            FLAC__STREAM_DECODER_INIT_STATUS_MEMORY_ALLOCATION_ERROR,
            FLAC__STREAM_DECODER_INIT_STATUS_ERROR_OPENING_FILE,
            FLAC__STREAM_DECODER_INIT_STATUS_ALREADY_INITIALIZED
        }

        internal enum FLAC__StreamDecoderReadStatus
        {
            FLAC__STREAM_DECODER_READ_STATUS_CONTINUE,
            FLAC__STREAM_DECODER_READ_STATUS_END_OF_STREAM,
            FLAC__STREAM_DECODER_READ_STATUS_ABORT
        }

        internal enum FLAC__StreamDecoderSeekStatus
        {
            FLAC__STREAM_DECODER_SEEK_STATUS_OK,
            FLAC__STREAM_DECODER_SEEK_STATUS_ERROR,
            FLAC__STREAM_DECODER_SEEK_STATUS_UNSUPPORTED
        }

        internal enum FLAC__StreamDecoderState
        {
            FLAC__STREAM_DECODER_SEARCH_FOR_METADATA,
            FLAC__STREAM_DECODER_READ_METADATA,
            FLAC__STREAM_DECODER_SEARCH_FOR_FRAME_SYNC,
            FLAC__STREAM_DECODER_READ_FRAME,
            FLAC__STREAM_DECODER_END_OF_STREAM,
            FLAC__STREAM_DECODER_OGG_ERROR,
            FLAC__STREAM_DECODER_SEEK_ERROR,
            FLAC__STREAM_DECODER_ABORTED,
            FLAC__STREAM_DECODER_MEMORY_ALLOCATION_ERROR,
            FLAC__STREAM_DECODER_UNINITIALIZED
        }

        internal enum FLAC__StreamDecoderTellStatus
        {
            FLAC__STREAM_DECODER_TELL_STATUS_OK,
            FLAC__STREAM_DECODER_TELL_STATUS_ERROR,
            FLAC__STREAM_DECODER_TELL_STATUS_UNSUPPORTED
        }

        internal enum FLAC__StreamDecoderWriteStatus
        {
            FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE,
            FLAC__STREAM_DECODER_WRITE_STATUS_ABORT
        }

        internal enum FLAC__StreamMetadata_Picture_Type
        {
            FLAC__STREAM_METADATA_PICTURE_TYPE_OTHER,
            FLAC__STREAM_METADATA_PICTURE_TYPE_FILE_ICON_STANDARD,
            FLAC__STREAM_METADATA_PICTURE_TYPE_FILE_ICON,
            FLAC__STREAM_METADATA_PICTURE_TYPE_FRONT_COVER,
            FLAC__STREAM_METADATA_PICTURE_TYPE_BACK_COVER,
            FLAC__STREAM_METADATA_PICTURE_TYPE_LEAFLET_PAGE,
            FLAC__STREAM_METADATA_PICTURE_TYPE_MEDIA,
            FLAC__STREAM_METADATA_PICTURE_TYPE_LEAD_ARTIST,
            FLAC__STREAM_METADATA_PICTURE_TYPE_ARTIST,
            FLAC__STREAM_METADATA_PICTURE_TYPE_CONDUCTOR,
            FLAC__STREAM_METADATA_PICTURE_TYPE_BAND,
            FLAC__STREAM_METADATA_PICTURE_TYPE_COMPOSER,
            FLAC__STREAM_METADATA_PICTURE_TYPE_LYRICIST,
            FLAC__STREAM_METADATA_PICTURE_TYPE_RECORDING_LOCATION,
            FLAC__STREAM_METADATA_PICTURE_TYPE_DURING_RECORDING,
            FLAC__STREAM_METADATA_PICTURE_TYPE_DURING_PERFORMANCE,
            FLAC__STREAM_METADATA_PICTURE_TYPE_VIDEO_SCREEN_CAPTURE,
            FLAC__STREAM_METADATA_PICTURE_TYPE_FISH,
            FLAC__STREAM_METADATA_PICTURE_TYPE_ILLUSTRATION,
            FLAC__STREAM_METADATA_PICTURE_TYPE_BAND_LOGOTYPE,
            FLAC__STREAM_METADATA_PICTURE_TYPE_PUBLISHER_LOGOTYPE,
            FLAC__STREAM_METADATA_PICTURE_TYPE_UNDEFINED
        }

        internal enum FLAC__SubframeType
        {
            FLAC__SUBFRAME_TYPE_CONSTANT,
            FLAC__SUBFRAME_TYPE_VERBATIM,
            FLAC__SUBFRAME_TYPE_FIXED,
            FLAC__SUBFRAME_TYPE_LPC
        }
    }
}
