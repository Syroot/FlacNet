﻿using System;
using System.IO;
using Syroot.FlacNet.Native;
using static Syroot.FlacNet.Native.LibFlac;

namespace Syroot.FlacNet
{
    /// <summary>
    /// Represents a wrapper around a FLAC stream decoder to simplify reading audio data.
    /// </summary>
    public unsafe class FlacReader : IDisposable
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _minTempBufferSamples = 1024;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly FLAC__StreamDecoder _decoder;
        private readonly bool _leaveOpen;
        private int _currentSample;

        // A temporary buffer used when more data was decoded by the FLAC stream than needed.
        private int[]? _tempBuffer;
        private int _firstTempSample;
        private int _lastTempSample;

        // A direct buffer used to write decoded data to if more data is needed than the one decoded by the FLAC stream.
        private Memory<int> _outBuffer;
        private int _outSamplesToRead;
        private int _samplesDecoded;

        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="FlacReader"/> class reading data from the file with the given
        /// <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to read data from.</param>
        public FlacReader(string fileName)
            : this(new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read)) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FlacReader"/> class reading data from the given
        /// <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read data from.</param>
        /// <param name="leaveOpen"><see langword="true"/> to not dispose the stream when disposing this instance.</param>
        public FlacReader(Stream stream, bool leaveOpen = false)
        {
            // Create native StreamDecoder.
            _decoder = FLAC__stream_decoder_new();
            FLAC__StreamDecoderInitStatus status = FLAC__stream_decoder_init_stream(_decoder, ReadCb,
                SeekCb, TellCb, LengthCb, EofCb, WriteCb, MetadataCb, ErrorCb,
                IntPtr.Zero);
            if (status != FLAC__StreamDecoderInitStatus.FLAC__STREAM_DECODER_INIT_STATUS_OK)
                throw new InvalidOperationException(status.ToString());

            BaseStream = stream;
            _leaveOpen = leaveOpen;

            // Read all metadata blocks.
            if (!FLAC__stream_decoder_process_until_end_of_metadata(_decoder))
                throw new InvalidOperationException(FLAC__stream_decoder_get_state(_decoder).ToString());
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the underlying <see cref="BaseStream"/> from which data is fetched.
        /// </summary>
        public Stream BaseStream { get; }

        /// <summary>
        /// Gets the number of bits required to store a single sample (bit depth).
        /// </summary>
        public int BitsPerSample { get; private set; }

        /// <summary>
        /// Gets the number of channels.
        /// </summary>
        public int Channels { get; private set; }

        /// <summary>
        /// Gets the number of samples per second.
        /// </summary>
        public int SampleRate { get; private set; }

        /// <summary>
        /// Gets the total number of samples.
        /// </summary>
        public int TotalSamples { get; private set; }

        /// <summary>
        /// Gets or sets the current sample from which data will be decoded on the next <see cref="ReadSamples"/> call.
        /// </summary>
        public int CurrentSample
        {
            get => _currentSample;
            set
            {
                _currentSample = value;
                if (!FLAC__stream_decoder_seek_absolute(_decoder, (ulong)_currentSample))
                    throw new InvalidOperationException("Could not set decoded position.");
            }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Reads samples into the given <paramref name="buffer"/> decoded as <see cref="Int32"/> values.
        /// </summary>
        /// <param name="buffer">The <see cref="Memory{Int32}"/> receiving decoded samples. The length must be divisible
        /// by the number of <see cref="Channels"/> to receive full samples.</param>
        /// <returns>The number of samples actually read.</returns>
        public int ReadSamples(Memory<int> buffer)
        {
            int samplesLeft = TotalSamples - _currentSample;
            int samplesToRead = Math.Min(samplesLeft, buffer.Length / Channels);

            // Directly transfer samples from the temporary buffer.
            if (_firstTempSample <= _currentSample && _currentSample < _lastTempSample)
            {
                int tempSamplesRemain = _lastTempSample - _currentSample;
                int tempSamplesToRead = Math.Min(samplesToRead, tempSamplesRemain);
                int samplesToCopy = Channels * tempSamplesToRead;
                _tempBuffer.AsMemory(Channels * (_currentSample - _firstTempSample), samplesToCopy).CopyTo(buffer);
                _currentSample += tempSamplesToRead;
                samplesToRead -= tempSamplesToRead;
                buffer = buffer.Slice(samplesToCopy);
            }

            // Request samples from the underlying stream.
            while (samplesToRead > 0)
            {
                _outBuffer = buffer; // Required to access the buffer in callback.
                _outSamplesToRead = samplesToRead;
                if (!FLAC__stream_decoder_process_single(_decoder))
                    throw new InvalidOperationException(FLAC__stream_decoder_get_state(_decoder).ToString());
                buffer = buffer.Slice(Channels * _samplesDecoded);
                samplesToRead -= _samplesDecoded;
                _currentSample += _samplesDecoded;
            }

            return samplesToRead;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose() => Dispose(true);

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Disposes native resources and optionally managed ones.
        /// </summary>
        /// <param name="disposing"><see langword="true"/> to dispose managed resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _decoder?.Dispose();
                    if (!_leaveOpen)
                        BaseStream?.Dispose();
                }

                _disposed = true;
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private FLAC__StreamDecoderReadStatus ReadCb(IntPtr decoder, byte* buffer, uint* bytes, IntPtr client_data)
        {
            int bytesToRead = (int)*bytes;
            if (bytesToRead <= 0)
                return FLAC__StreamDecoderReadStatus.FLAC__STREAM_DECODER_READ_STATUS_ABORT;

            int bytesRead;
            try
            {
                Span<byte> bufferSpan = new Span<byte>(buffer, bytesToRead);
#if NETCOREAPP
                bytesRead = BaseStream.Read(bufferSpan);
#else
                byte[] bytesTemp = new byte[bytesToRead];
                bytesRead = BaseStream.Read(bytesTemp, 0, bytesToRead);
                bytesTemp.CopyTo(bufferSpan);
#endif
            }
            catch
            {
                return FLAC__StreamDecoderReadStatus.FLAC__STREAM_DECODER_READ_STATUS_ABORT;
            }

            *bytes = (uint)bytesRead;
            return bytesRead == 0
                ? FLAC__StreamDecoderReadStatus.FLAC__STREAM_DECODER_READ_STATUS_END_OF_STREAM
                : FLAC__StreamDecoderReadStatus.FLAC__STREAM_DECODER_READ_STATUS_CONTINUE;
        }

        private FLAC__StreamDecoderSeekStatus SeekCb(IntPtr decoder, ulong absolute_byte_offset, IntPtr client_data)
        {
            if (!BaseStream.CanSeek)
                return FLAC__StreamDecoderSeekStatus.FLAC__STREAM_DECODER_SEEK_STATUS_UNSUPPORTED;

            try
            {
                BaseStream.Position = (long)absolute_byte_offset;
            }
            catch
            {
                return FLAC__StreamDecoderSeekStatus.FLAC__STREAM_DECODER_SEEK_STATUS_ERROR;
            }

            return FLAC__StreamDecoderSeekStatus.FLAC__STREAM_DECODER_SEEK_STATUS_OK;
        }

        private FLAC__StreamDecoderTellStatus TellCb(IntPtr decoder, ulong* absolute_byte_offset, IntPtr client_data)
        {
            if (!BaseStream.CanSeek)
                return FLAC__StreamDecoderTellStatus.FLAC__STREAM_DECODER_TELL_STATUS_UNSUPPORTED;

            try
            {
                *absolute_byte_offset = (ulong)BaseStream.Position;
            }
            catch
            {
                return FLAC__StreamDecoderTellStatus.FLAC__STREAM_DECODER_TELL_STATUS_ERROR;
            }

            return FLAC__StreamDecoderTellStatus.FLAC__STREAM_DECODER_TELL_STATUS_OK;
        }

        private FLAC__StreamDecoderLengthStatus LengthCb(IntPtr decoder, ulong* stream_length, IntPtr client_data)
        {
            if (!BaseStream.CanSeek)
                return FLAC__StreamDecoderLengthStatus.FLAC__STREAM_DECODER_LENGTH_STATUS_UNSUPPORTED;

            try
            {
                *stream_length = (ulong)BaseStream.Length;
            }
            catch
            {
                return FLAC__StreamDecoderLengthStatus.FLAC__STREAM_DECODER_LENGTH_STATUS_ERROR;
            }

            return FLAC__StreamDecoderLengthStatus.FLAC__STREAM_DECODER_LENGTH_STATUS_OK;
        }

        private bool EofCb(IntPtr decoder, IntPtr client_data)
        {
            try
            {
                return BaseStream.Length <= BaseStream.Position;
            }
            catch
            {
                return false;
            }
        }

        private FLAC__StreamDecoderWriteStatus WriteCb(IntPtr decoder, FLAC__Frame* frame, int** buffer,
            IntPtr client_data)
        {
            int samplesRemain = (int)frame->header.blocksize;

            // Write samples directly to out buffer, if available.
            int outSamples = 0;
            if (!_outBuffer.IsEmpty)
            {
                outSamples = Math.Min(_outSamplesToRead, samplesRemain);
                TransferInt32Samples(buffer, 0, outSamples, _outBuffer.Span);
                _outBuffer = new Memory<int>();
                _outSamplesToRead = 0;
                _samplesDecoded = outSamples;
                samplesRemain -= outSamples;
            }

            // If samples are still available in the FLAC frame, store them in the temporary buffer.
            if (samplesRemain > 0)
            {
                int tempSamples = Channels * samplesRemain;
                if (_tempBuffer == null || _tempBuffer.Length < tempSamples)
                    _tempBuffer = new int[Math.Max(_minTempBufferSamples, tempSamples)];
                TransferInt32Samples(buffer, outSamples, samplesRemain, _tempBuffer);
                _firstTempSample = _currentSample + outSamples;
                _lastTempSample = _firstTempSample + samplesRemain;
            }

            return FLAC__StreamDecoderWriteStatus.FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
        }

        private void TransferInt32Samples(int** buffer, int offset, int count, Span<int> outBuffer)
        {
            for (int i = 0; i < count; i++)
                for (int j = 0; j < Channels; j++)
                    outBuffer[i * Channels + j] = buffer[j][i + offset];
        }

        private void MetadataCb(IntPtr decoder, FLAC__StreamMetadata metadata, IntPtr client_data)
        {
            if (metadata.type == FLAC__MetadataType.FLAC__METADATA_TYPE_STREAMINFO)
            {
                FLAC__StreamMetadata_StreamInfo streamInfo = metadata.stream_info;
                BitsPerSample = (int)streamInfo.bits_per_sample;
                Channels = (int)streamInfo.channels;
                SampleRate = (int)streamInfo.sample_rate;
                TotalSamples = (int)streamInfo.total_samples;
            }
        }

        private void ErrorCb(IntPtr decoder, FLAC__StreamDecoderErrorStatus status, IntPtr client_data)
            => throw new InvalidOperationException(status.ToString());
    }
}
