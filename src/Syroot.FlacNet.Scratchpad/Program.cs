﻿using System.IO;

namespace Syroot.FlacNet.Scratchpad
{
    internal class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main()
        {
            using Stream stream = new FileStream(@"C:\Users\Ray\Pictures\happy_new_year.flac", FileMode.Open,
                FileAccess.Read, FileShare.Read);
            using FlacReader reader = new FlacReader(stream);
            int[] samples = new int[100];
            reader.ReadSamples(samples);
            reader.CurrentSample = 200;
            reader.ReadSamples(samples);
        }
    }
}
